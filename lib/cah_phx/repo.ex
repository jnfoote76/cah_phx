defmodule CahPhx.Repo do
  use Ecto.Repo,
    otp_app: :cah_phx,
    adapter: Ecto.Adapters.Postgres
end
