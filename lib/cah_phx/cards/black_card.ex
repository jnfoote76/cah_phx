defmodule CahPhx.Cards.BlackCard do
  use Ecto.Schema
  alias CahPhx.Cards.CardSet

  schema "black_cards" do
    field(:draw, :integer)
    field(:pick, :integer)
    field(:text, :string)
    field(:watermark, :string)

    many_to_many(:card_sets, CardSet, join_through: "card_set_black_card", on_replace: :delete)
  end
end
