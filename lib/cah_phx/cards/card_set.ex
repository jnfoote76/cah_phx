defmodule CahPhx.Cards.CardSet do
  use Ecto.Schema
  alias CahPhx.Cards.BlackCard
  alias CahPhx.Cards.WhiteCard
  alias CahPhx.Cards.CardSetBlackCard
  alias CahPhx.Cards.CardSetWhiteCard

  schema "card_sets" do
    field(:active, :boolean)
    field(:base_deck, :boolean)
    field(:description, :string)
    field(:name, :string)
    field(:weight, :integer)

    many_to_many(:black_cards, BlackCard,
      join_through: CardSetBlackCard,
      on_replace: :delete
    )

    many_to_many(:white_cards, WhiteCard,
      join_through: CardSetWhiteCard,
      on_replace: :delete
    )
  end
end
