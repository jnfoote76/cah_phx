defmodule CahPhx.Cards do
  import Ecto.Query, warn: false

  alias CahPhx.Repo
  alias CahPhx.Cards.CardSet

  def get_card_sets(card_set_ids) do
    Repo.all(
      from cs in CardSet,
        left_join: b in assoc(cs, :black_cards),
        left_join: w in assoc(cs, :white_cards),
        where: cs.id in ^card_set_ids,
        preload: [black_cards: b, white_cards: w]
    )
  end
end
