defmodule CahPhx.Cards.WhiteCard do
  use Ecto.Schema
  alias CahPhx.Cards.CardSet

  schema "white_cards" do
    field(:text, :string)
    field(:watermark, :string)

    many_to_many(:card_sets, CardSet, join_through: "card_set_white_card", on_replace: :delete)
  end
end
