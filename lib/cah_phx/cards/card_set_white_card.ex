defmodule CahPhx.Cards.CardSetWhiteCard do
  use Ecto.Schema

  alias CahPhx.Cards.CardSet
  alias CahPhx.Cards.WhiteCard

  schema "card_set_white_card" do
    belongs_to :card_set, CardSet
    belongs_to :white_card, WhiteCard
  end
end
