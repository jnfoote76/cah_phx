defmodule CahPhx.Cards.CardSetBlackCard do
  use Ecto.Schema

  alias CahPhx.Cards.CardSet
  alias CahPhx.Cards.BlackCard

  schema "card_set_black_card" do
    belongs_to :card_set, CardSet
    belongs_to :black_card, BlackCard
  end
end
