defmodule CahPhx.Game.Settings do
  alias CahPhx.Game.Timeout

  defstruct card_set_ids: [],
            timeout: %Timeout{},
            base_num_cards_in_hand: 7,
            winning_score: 10
end
