defmodule CahPhx.Game.Timeout do
  defstruct play_cards: 60,
            judging: 60,
            extra_time_for_doubles: 15
end
