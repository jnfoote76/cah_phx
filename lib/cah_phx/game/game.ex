defmodule CahPhx.Game do
  use GenServer

  alias CahPhx.Cards
  alias CahPhx.Cards.WhiteCard
  alias CahPhx.Cards.CardSet
  alias CahPhx.Game
  alias CahPhx.Game.Settings

  require Logger

  defstruct player_hands: %{},
            player_scores: %{},
            leading_score: 0,
            new_players: [],
            white_cards_left: [],
            black_cards_left: [],
            white_cards_played: [],
            black_cards_played: [],
            current_black_card: nil,
            current_white_cards: %{},
            prev_czars: [],
            owner: nil,
            current_czar: nil,
            upcoming_czars: [],
            state: :not_started,
            settings: %Settings{}

  @card_hand_size Application.get_env(:cah_phx, __MODULE__)[:num_cards_in_hand]

  def start_link(state) do
    GenServer.start_link(__MODULE__, state)
  end

  def init(%Game{} = state) do
    {:ok, state}
  end

  def init(owner_name) when is_binary(owner_name) do
    state =
      %Game{}
      |> add_player(owner_name)
      |> set_owner(owner_name)

    {:ok, state}
  end

  def handle_call(:get_state, _from, state) do
    {:reply, state, state}
  end

  def handle_call({:add_player, name}, _from, state) do
    %Game{player_scores: player_scores} = new_state = add_player(state, name)

    {:reply, {:ok, player_scores}, new_state}
  end

  # Action: remove player from 'player_hands', 'player_scores', and czars list(s)
  # Returns: map containing all players' scores
  # Constraints:
  #   * If 'name' is room owner, game ends
  #   * If 'name' is current card czar, round ends, cards are returned to players' hands, and new round starts
  def handle_call({:remove_player, _name}, _from, state) do
    {:reply, {:error, :unimplemented}, state}
  end

  # Action: initialize card deck, start new hand
  # Constraints:
  #   * 'player_name' must be owner
  #   * 'state' must be :not_started
  def handle_call({:start_game, player_name, %Settings{} = settings}, _from, %Game{} = game_state) do
    {ret, new_game_state} =
      cond do
        player_name == game_state.owner ->
          cond do
            game_state.state == :not_started ->
              card_czar_order =
                game_state.player_scores
                |> Map.keys()
                |> Enum.shuffle()

              game_state
              |> Map.put(:settings, settings)
              |> Map.put(:upcoming_czars, card_czar_order)
              |> initialize_deck()
              |> start_new_hand()

            true ->
              {{:error, "Game is already started"}, game_state}
          end

        true ->
          {{:error, "Not authorized"}, game_state}
      end

    {:reply, ret, new_game_state}
  end

  # Action: change state to :not_started, reset hands and scores
  # Constraints:
  #   * 'player_name' must be game owner
  def handle_call({:end_game, _player_name}, _from, state) do
    {:reply, {:error, :unimplemented}, state}
  end

  # Just start a new hand...
  def handle_call(:start_new_hand, _from, %Game{} = state) do
    {ret, new_state} = start_new_hand(state)
    {:reply, ret, new_state}
  end

  # Action: Add item to 'white_cards_played' containing the card and who played it, remove card from player's hand
  # Returns: map indicating who has/hasn't played yet
  # Constraints:
  #   * 'state' must be :players_submitting
  #   * 'player_name' must not be current card czar
  #   * Card must be in player's hand
  def handle_call({:play_white_card, _player_name, _card_id}, _from, %Game{} = state) do
    {:reply, {:error, :unimplemented}, state}
  end

  # Action: increment score of whoever played card
  # Constraints:
  #   * 'state' must be :judging
  #   * Card must actually be in play
  #   * 'player_name' must be current card czar
  def handle_call({:pick_winning_card, _player_name, _card_id}, _from, state) do
    {:reply, {:error, :unimplemented}, state}
  end

  defp add_player(%Game{} = game, name) do
    player_hands = Map.put(game.player_hands, name, %{})
    player_scores = Map.put(game.player_scores, name, 0)

    game
    |> Map.put(:player_hands, player_hands)
    |> Map.put(:player_scores, player_scores)
  end

  defp set_owner(%Game{} = game, owner_name) do
    Map.put(game, :owner, owner_name)
  end

  defp initialize_deck(%Game{settings: %Settings{card_set_ids: card_set_ids}} = state) do
    card_sets =
      card_set_ids
      |> Cards.get_card_sets()

    black_cards =
      List.foldr(card_sets, [], fn %CardSet{black_cards: black_cards}, acc ->
        acc ++ black_cards
      end)
      |> Enum.shuffle()

    white_cards =
      List.foldr(card_sets, [], fn %CardSet{white_cards: white_cards}, acc ->
        acc ++ white_cards
      end)
      |> Enum.shuffle()

    Logger.debug("Stopping here")

    state
    |> Map.put(:black_cards_left, black_cards)
    |> Map.put(:white_cards_left, white_cards)
  end

  defp start_new_hand(%Game{} = state) do
    Logger.debug("Stopping here")

    case deal_to_everyone(state.player_hands, state.white_cards_left) do
      {player_hands, white_cards_left} ->
        case state.black_cards_left do
          [] ->
            {{:error, "Ran out of black cards"}, state}

          [new_black_card | black_cards_left] ->
            {current_czar, prev_czars, upcoming_czars} =
              assign_new_card_czar(
                state.current_czar,
                state.prev_czars,
                state.upcoming_czars
              )

            new_state =
              state
              |> Map.put(:player_hands, player_hands)
              |> Map.put(:white_cards_left, white_cards_left)
              |> Map.put(:current_black_card, new_black_card)
              |> Map.put(:black_cards_left, black_cards_left)
              |> Map.put(:current_czar, current_czar)
              |> Map.put(:prev_czars, prev_czars)
              |> Map.put(:upcoming_czars, upcoming_czars)
              |> Map.put(:state, :players_submitting)

            {{:ok, player_hands, new_black_card, current_czar}, new_state}
        end

      :not_enough_cards ->
        {{:error, "Ran out of white cards"}, state}
    end
  end

  defp deal_to_everyone(player_hands, white_cards_left) do
    player_names = Map.keys(player_hands)

    List.foldr(player_names, {player_hands, white_cards_left}, fn name, acc ->
      case acc do
        {player_hands, white_cards_left} ->
          curr_player_hand = Map.get(player_hands, name, %{})

          num_cards_in_hand =
            curr_player_hand
            |> Map.keys()
            |> Kernel.length()

          case deal_white_cards(
                 curr_player_hand,
                 white_cards_left,
                 @card_hand_size - num_cards_in_hand
               ) do
            {curr_player_hand, white_cards_left} ->
              {Map.put(player_hands, name, curr_player_hand), white_cards_left}

            :not_enough_cards ->
              :not_enough_cards
          end

        :not_enough_cards ->
          :not_enough_cards
      end
    end)
  end

  defp deal_white_cards(hand, available_white_cards, 1) do
    deal_white_card(hand, available_white_cards)
  end

  defp deal_white_cards(hand, available_white_cards, n) do
    case deal_white_card(hand, available_white_cards) do
      {hand, available_white_cards} ->
        deal_white_cards(hand, available_white_cards, n - 1)

      :no_more_cards ->
        :not_enough_cards
    end
  end

  defp deal_white_card(_hand, []) do
    :no_more_cards
  end

  defp deal_white_card(hand, [
         %WhiteCard{id: first_card_id} = first_white_card | other_white_cards
       ]) do
    {Map.put(hand, first_card_id, first_white_card), other_white_cards}
  end

  defp assign_new_card_czar(nil, _prev_czars, []) do
    nil
  end

  defp assign_new_card_czar(nil, _prev_czars, [first_czar | remaining_czars]) do
    {first_czar, [], remaining_czars}
  end

  defp assign_new_card_czar(current_czar, [], []) do
    {current_czar, [], []}
  end

  defp assign_new_card_czar(current_czar, [first_czar | remaining_czars], []) do
    {first_czar, [], remaining_czars ++ current_czar}
  end

  defp assign_new_card_czar(current_czar, prev_czars, [next_czar | remaining_czars]) do
    {next_czar, prev_czars ++ current_czar, remaining_czars}
  end
end
