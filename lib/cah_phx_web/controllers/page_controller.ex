defmodule CahPhxWeb.PageController do
  use CahPhxWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
