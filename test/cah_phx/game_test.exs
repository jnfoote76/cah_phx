defmodule CahPhx.GameTest do
  use CahPhxWeb.ConnCase

  alias CahPhx.Game
  alias CahPhx.Game.Settings
  alias CahPhx.Cards.{BlackCard, WhiteCard, CardSet}
  alias CahPhx.Repo

  describe "start_link/1" do
    test "When Game struct passed as parameter, initializes using parameter as state" do
      {:ok, game_pid} = Game.start_link(%Game{})
      assert %Game{} = GenServer.call(game_pid, :get_state)

      {:ok, game_pid} = Game.start_link(%Game{player_scores: %{"Brian" => 5}})
      assert %Game{player_scores: %{"Brian" => 5}} = GenServer.call(game_pid, :get_state)
    end

    test "When string (player name) passed as parameter, initializes empty Game with player name added as a player and owner" do
      {:ok, game_pid} = Game.start_link("Steve")

      assert %Game{
               owner: "Steve",
               player_scores: %{"Steve" => 0},
               player_hands: %{"Steve" => %{}}
             } = GenServer.call(game_pid, :get_state)

      {:ok, game_pid} = Game.start_link("Charles")

      assert %Game{
               owner: "Charles",
               player_scores: %{"Charles" => 0},
               player_hands: %{"Charles" => %{}}
             } = GenServer.call(game_pid, :get_state)
    end
  end

  describe "handle_call.add_player/1" do
    test "adds player and initializes their score and hand" do
      {:ok, game_pid} = Game.start_link(%Game{})

      {:ok, player_scores} = GenServer.call(game_pid, {:add_player, "Harry"})
      harry_score = Map.get(player_scores, "Harry")
      assert harry_score == 0

      {:ok, player_scores} = GenServer.call(game_pid, {:add_player, "Sally"})
      sally_score = Map.get(player_scores, "Sally")
      assert sally_score == 0

      %Game{player_scores: player_scores, player_hands: player_hands} =
        GenServer.call(game_pid, :get_state)

      assert %{"Harry" => 0, "Sally" => 0} = player_scores
      assert %{"Harry" => %{}, "Sally" => %{}} = player_hands
    end
  end

  describe "handle_call.start_game/2 (single normal card set)" do
    setup [:create_first_normal_card_set]

    test "works when input is valid", %{
      first_normal_set_id: first_normal_set_id
    } do
      state = %Game{
        owner: "Harry",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :not_started
      }

      settings = %Settings{
        card_set_ids: [first_normal_set_id]
      }

      {:ok, game_pid} = Game.start_link(state)

      assert {:ok, %{"Harry" => harry_hand, "Ron" => ron_hand, "Hermione" => hermione_hand},
              %BlackCard{},
              _card_czar} = GenServer.call(game_pid, {:start_game, "Harry", settings})

      assert Kernel.length(Map.keys(harry_hand)) == 7
      assert Kernel.length(Map.keys(hermione_hand)) == 7
      assert Kernel.length(Map.keys(ron_hand)) == 7

      assert %Game{
               player_hands: %{
                 "Harry" => harry_hand_state,
                 "Ron" => ron_hand_state,
                 "Hermione" => hermione_hand_state
               },
               current_black_card: %BlackCard{},
               black_cards_left: black_cards_left,
               white_cards_left: white_cards_left,
               state: :players_submitting
             } = GenServer.call(game_pid, :get_state)

      assert harry_hand_state == harry_hand
      assert hermione_hand_state == hermione_hand
      assert ron_hand_state == ron_hand

      assert Kernel.length(black_cards_left) == 9
      assert Kernel.length(white_cards_left) == 29
    end

    test "returns error and doesn't modify game state if game already started", %{
      first_normal_set_id: first_normal_set_id
    } do
      state = %Game{
        owner: "Harry",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :players_submitting
      }

      settings = %Settings{
        card_set_ids: [first_normal_set_id]
      }

      {:ok, game_pid} = Game.start_link(state)

      assert {:error, "Game is already started"} =
               GenServer.call(game_pid, {:start_game, "Harry", settings})

      assert state == GenServer.call(game_pid, :get_state)
    end

    test "returns error and doesn't modify game state if anyone other than the owner attempts to start the game",
         %{
           first_normal_set_id: first_normal_set_id
         } do
      state_harry_owner = %Game{
        owner: "Harry",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :not_started
      }

      settings = %Settings{
        card_set_ids: [first_normal_set_id]
      }

      {:ok, game_pid_harry_owner} = Game.start_link(state_harry_owner)

      assert {:error, "Not authorized"} =
               GenServer.call(game_pid_harry_owner, {:start_game, "Ron", settings})

      assert state_harry_owner == GenServer.call(game_pid_harry_owner, :get_state)

      state_ron_owner = %Game{
        owner: "Ron",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :not_started
      }

      {:ok, game_pid_ron_owner} = Game.start_link(state_ron_owner)

      assert {:error, "Not authorized"} =
               GenServer.call(game_pid_ron_owner, {:start_game, "Harry", settings})

      assert state_ron_owner == GenServer.call(game_pid_ron_owner, :get_state)
    end
  end

  describe "handle_call.start_game/2 (single set, not enough white cards for first hand)" do
    setup [:create_card_set_not_enough_white_cards]

    test "starting game returns error", %{
      not_enough_white_cards_set_id: not_enough_white_cards_set_id
    } do
      state = %Game{
        owner: "Harry",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :not_started
      }

      settings = %Settings{
        card_set_ids: [not_enough_white_cards_set_id]
      }

      {:ok, game_pid} = Game.start_link(state)

      assert {:error, "Ran out of white cards"} =
               GenServer.call(game_pid, {:start_game, "Harry", settings})
    end
  end

  describe "handle_call.start_game/2 (single set, no black cards)" do
    setup [:create_card_set_not_enough_black_cards]

    test "starting game returns error", %{
      not_enough_black_cards_set_id: not_enough_black_cards_set_id
    } do
      state = %Game{
        owner: "Harry",
        player_scores: %{"Harry" => 0, "Ron" => 0, "Hermione" => 0},
        player_hands: %{"Harry" => %{}, "Ron" => %{}, "Hermione" => %{}},
        state: :not_started
      }

      settings = %Settings{
        card_set_ids: [not_enough_black_cards_set_id]
      }

      {:ok, game_pid} = Game.start_link(state)

      assert {:error, "Ran out of black cards"} =
               GenServer.call(game_pid, {:start_game, "Harry", settings})
    end
  end

  defp create_first_normal_card_set(_) do
    %CardSet{id: first_normal_set_id} = first_normal_set = Repo.insert!(%CardSet{})

    Enum.map(1..10, fn _n ->
      Repo.insert!(%BlackCard{card_sets: [first_normal_set]})
    end)

    Enum.map(1..50, fn _n ->
      Repo.insert!(%WhiteCard{card_sets: [first_normal_set]})
    end)

    {:ok, first_normal_set_id: first_normal_set_id}
  end

  defp create_card_set_not_enough_white_cards(_) do
    %CardSet{id: not_enough_white_cards_set_id} =
      not_enough_white_cards_set = Repo.insert!(%CardSet{})

    Enum.map(1..10, fn _n ->
      Repo.insert!(%BlackCard{card_sets: [not_enough_white_cards_set]})
    end)

    Enum.map(1..10, fn _n ->
      Repo.insert!(%WhiteCard{card_sets: [not_enough_white_cards_set]})
    end)

    {:ok, not_enough_white_cards_set_id: not_enough_white_cards_set_id}
  end

  defp create_card_set_not_enough_black_cards(_) do
    %CardSet{id: not_enough_black_cards_set_id} =
      not_enough_black_cards_set = Repo.insert!(%CardSet{})

    Enum.map(1..50, fn _n ->
      Repo.insert!(%WhiteCard{card_sets: [not_enough_black_cards_set]})
    end)

    {:ok, not_enough_black_cards_set_id: not_enough_black_cards_set_id}
  end
end
