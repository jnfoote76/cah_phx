defmodule CahPhx.Repo.Migrations.CreateCardSetsTable do
  use Ecto.Migration

  def change do
    create table(:card_sets) do
      add :active, :boolean
      add :base_deck, :boolean
      add :description, :string
      add :name, :string
      add :weight, :integer
    end
  end
end
