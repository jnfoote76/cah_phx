defmodule CahPhx.Repo.Migrations.CreateCardSetWhiteCard do
  use Ecto.Migration

  def change do
    create table(:card_set_white_card) do
      add :card_set_id, references(:card_sets)
      add :white_card_id, references(:white_cards)
    end
  end
end
