defmodule CahPhx.Repo.Migrations.CreateBlackCardsTable do
  use Ecto.Migration

  def change do
    create table(:black_cards) do
      add :draw, :integer
      add :pick, :integer
      add :text, :string
      add :watermark, :string
    end
  end
end
