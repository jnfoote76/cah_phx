defmodule CahPhx.Repo.Migrations.CreateCardSetBlackCard do
  use Ecto.Migration

  def change do
    create table(:card_set_black_card) do
      add :card_set_id, references(:card_sets)
      add :black_card_id, references(:black_cards)
    end
  end
end
