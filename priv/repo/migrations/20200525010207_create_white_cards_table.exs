defmodule CahPhx.Repo.Migrations.CreateWhiteCardsTable do
  use Ecto.Migration

  def change do
    create table(:white_cards) do
      add :text, :string
      add :watermark, :string
    end
  end
end
