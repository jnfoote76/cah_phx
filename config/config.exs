# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :cah_phx,
  ecto_repos: [CahPhx.Repo]

# Configures the endpoint
config :cah_phx, CahPhxWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "N7hCvYeEOemNSJHEuPbIaOGfqcxwnAZisDBSH3C/+coJ7zwo6Mq4PetAVpLPLFOK",
  render_errors: [view: CahPhxWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: CahPhx.PubSub,
  live_view: [signing_salt: "S30esxeQ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :cah_phx, CahPhx.Game, num_cards_in_hand: 7

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
